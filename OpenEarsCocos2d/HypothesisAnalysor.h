//
//  HypothesisAnalysor.h
//  OpenEarsCocos2d
//
//  Created by Dahiri Farid on 11/26/13.
//  Copyright (c) 2013 Dahiri Farid. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HypothesisAnalysor : NSObject

+ (BOOL)analyseHypothesisArray:(NSArray *)hypothesisArray;

@end
