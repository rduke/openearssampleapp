//
//  HypothesisAnalysor.m
//  OpenEarsCocos2d
//
//  Created by Dahiri Farid on 11/26/13.
//  Copyright (c) 2013 Dahiri Farid. All rights reserved.
//

#import "HypothesisAnalysor.h"

@implementation HypothesisAnalysor


+ (BOOL)analyseHypothesisArray:(NSArray *)hypothesisArray
{
    if (hypothesisArray.count == 0)
        return NO;
    
    NSInteger rejectedCount = 0;
    NSInteger averageScore = 0;
    
    BOOL isFisrtRejected = NO;
    
    for (NSDictionary* hypothesis in hypothesisArray)
    {
        NSString* hypothesisText = hypothesis[@"Hypothesis"];
        if ([hypothesisText rangeOfString:@"REJ"].location != NSNotFound)
        {
            rejectedCount++;
            if ([hypothesisArray indexOfObject:hypothesisText] == 0)
                isFisrtRejected = YES;
        }
        
        averageScore += [hypothesis[@"Score"] integerValue];
    }
    averageScore = averageScore / hypothesisArray.count;

    NSLog(@"Average Score %d", averageScore);
    NSLog(@"Rejected count %d", rejectedCount);
    
    return (hypothesisArray.count - rejectedCount) > 1 && !isFisrtRejected;
}


@end
