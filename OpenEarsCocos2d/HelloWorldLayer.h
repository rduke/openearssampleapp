//
//  HelloWorldLayer.h
//  OpenEarsCocos2d
//
//  Created by Dahiri Farid on 11/24/13.
//  Copyright Dahiri Farid 2013. All rights reserved.
//


#import <GameKit/GameKit.h>

// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"
#import <OpenEars/OpenEarsEventsObserver.h>
#import <Slt/Slt.h>

@class PocketsphinxController;
@class FliteController;
// HelloWorldLayer
@interface HelloWorldLayer : CCLayer<OpenEarsEventsObserverDelegate>
{
    Slt *slt;
    
	// These three are important OpenEars classes that ViewController demonstrates the use of. There is a fourth important class (LanguageModelGenerator) demonstrated
	// inside the ViewController implementation in the method viewDidLoad.
	
	OpenEarsEventsObserver *openEarsEventsObserver; // A class whose delegate methods which will allow us to stay informed of changes in the Flite and Pocketsphinx statuses.
	PocketsphinxController *pocketsphinxController; // The controller for Pocketsphinx (voice recognition).
	FliteController *fliteController; // The controller for Flite (speech).
    
    BOOL usingStartLanguageModel;
	
	// Strings which aren't required for OpenEars but which will help us show off the dynamic language features in this sample app.
	NSString *pathToGrammarToStartAppWith;
	NSString *pathToDictionaryToStartAppWith;
	
	NSString *pathToDynamicallyGeneratedGrammar;
	NSString *pathToDynamicallyGeneratedDictionary;
    
	
	// Our NSTimer that will help us read and display the input and output levels without locking the UI
	NSTimer *uiUpdateTimer;
}

// Example for reading out the input audio levels without locking the UI using an NSTimer

- (void) startDisplayingLevels;
- (void) stopDisplayingLevels;

// These three are the important OpenEars objects that this class demonstrates the use of.
@property (nonatomic, strong) Slt *slt;

@property (nonatomic, strong) OpenEarsEventsObserver *openEarsEventsObserver;
@property (nonatomic, strong) PocketsphinxController *pocketsphinxController;
@property (nonatomic, strong) FliteController *fliteController;


@property (nonatomic, assign) BOOL usingStartLanguageModel;

// Things which help us show off the dynamic language features.
@property (nonatomic, copy) NSString *pathToGrammarToStartAppWith;
@property (nonatomic, copy) NSString *pathToDictionaryToStartAppWith;
@property (nonatomic, copy) NSString *pathToDynamicallyGeneratedGrammar;
@property (nonatomic, copy) NSString *pathToDynamicallyGeneratedDictionary;

// UI
@property (nonatomic, retain) CCLabelTTF *statusLabel;
@property (nonatomic, retain) CCLabelTTF *pocketsphinxDbLabel;
@property (nonatomic, retain) CCLabelTTF *fliteDbLabel;
@property (nonatomic, retain) CCLabelTTF *heardTextLabel;
@property (nonatomic, retain) CCMenuItemFont *startRecognitionBtn;
@property (nonatomic, retain) CCMenuItemFont *stopRecognitionBtn;
@property (nonatomic, retain) CCMenuItemFont *resumeRecognitionBtn;
@property (nonatomic, retain) CCMenuItemFont *suspendRecognitionBtn;


// Our NSTimer that will help us read and display the input and output levels without locking the UI
@property (nonatomic, strong) 	NSTimer *uiUpdateTimer;

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;

@end
